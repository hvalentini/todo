/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';
import Todo from './src/components/Todo.js'
import Estiloso from './src/components/Estiloso.js'

const Main = () => (<Estiloso />)

AppRegistry.registerComponent('AwesomeProject', () => Main);
