import { StyleSheet } from 'react-native';

const opacity = .5
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'red'
  },
  topo: {
    flex: .3,
    backgroundColor: `rgba(0,0,0,${opacity})`
  },
  fundo: {
    flex: .7,
    backgroundColor: 'rgba(255,255,255,.3)'
  },
  text: {
    color: '#fff'
  },
  destaque: {
    position: 'absolute',
    right: 40,
    backgroundColor: '#3e3',
    borderRadius: 50,
    top: 40
  },
})
