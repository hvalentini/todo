import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';
import { styles } from './styles'

export default class Estiloso extends Component {
  constructor(props) {
    super(props)
  }
  render () {
    const {height, width} = Dimensions.get('window');
    const destaqueEstilo = { width: width/2, height: height/3 }
    return (
      <View style={styles.container}>
        <View style={styles.topo}>
          <Text style={styles.text}>Estiloso 2</Text>
        </View>
        <View style={styles.fundo}>
          <Text style={styles.text}>Estiloso</Text>
        </View>
        <View style={[styles.destaque, destaqueEstilo]}>
        </View>
      </View>
    )
  }
}
