import { StyleSheet } from 'react-native';

const opacity = .5
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'red'
  },
  topo: {
    flex: .1,
    backgroundColor: `rgba(0,0,0,${opacity})`
  },
  fundo: {
    flex: .9,
    backgroundColor: '#fafafa'
  },
  text: {
    color: '#fff'
  },
  destaque: {
    position: 'absolute',
    right: 40,
    backgroundColor: '#f0f',
    borderRadius: 0,
    top: 150
  },
})
