import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';

export default class Todo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      todos: [],
      newTodo: '',
      test: ''
    }
    this.handleChangeText = this.handleChangeText.bind(this);
    this.handlePress = this.handlePress.bind(this);
  }
  componentWillMount() {
    fetch('http://10.20.33.145:3000/todos', {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then(response => response.json())
    .then((responseJson) => {
      console.log(responseJson)
      this.setState({todos: responseJson})
    })
    .catch((error) => {
      console.error(error)
    })

  }
  handleChangeText (text) {
    this.setState({ newTodo: text})
  }
  handlePress () {
    const newTodo = { name: this.state.newTodo }
    fetch('http://10.20.33.145:3000/todos', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newTodo)
    })
    .then(response => response.json())
    .then((responseJson) => {
      const todos = [responseJson, ...this.state.todos]
      this.setState({ todos, newTodo: '' })
    })
    .catch((error) => {
      console.error(error)
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>
          Minhas atividades
        </Text>
        <View style={styles.form}>
          <TextInput onChangeText={this.handleChangeText}
          value={this.state.newTodo}
          style={styles.input}>
          </TextInput>
          <TouchableOpacity
            style={styles.button}
            onPress={this.handlePress}>
            <Text style={styles.buttonText}>Criar</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.todo}>
          {
            this.state.todos.map((todo, i) => <Text key={i} style={styles.todoText}>{todo.id} {todo.name}</Text>)
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.15)'
  },
  title: {
    color: 'white',
    backgroundColor: 'peru',
    fontSize: 24,
    fontWeight: '300',
    textAlign: 'center',
    padding: 20
  },
  titleImportant: {
    marginTop: 20,
    backgroundColor: 'red'
  },
  form: {
    flexDirection: 'row'
  },
  button: {
    backgroundColor: '#00f',
    width: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: 'rgba(255,255,255,.87)',
    fontSize: 24
  },
  input: {
    flex: 1,
    marginRight: 15,
    fontSize: 24
  },
  todo: {
    marginTop: 25,
    padding: 20
  },
  todoText: {
    fontSize: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    marginBottom: 15
  }
})
